package com.progressoft.interns.main;

import com.progressoft.interns.filecreator.FileCreator;
import com.progressoft.interns.filecreator.SampleTextCreator;

import java.nio.file.Path;
import java.util.List;

public class Main {

    public static void main(String[] args) {
     FileCreator fileCreator = new SampleTextCreator();
     List<Path> filesCreated = fileCreator.createFile();
     filesCreated.forEach(path -> System.out.println("File Created at: " + path.toString()));
    }

}
