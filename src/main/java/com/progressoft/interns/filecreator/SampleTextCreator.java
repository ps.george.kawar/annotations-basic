package com.progressoft.interns.filecreator;

import com.progressoft.interns.filecreator.annotation.DefaultPath;
import com.progressoft.interns.filecreator.annotation.RunnableMethod;

public class SampleTextCreator extends FileCreator{

    @RunnableMethod
    @DefaultPath(value = "CHANGE ME /home/user/Desktop/SamplePath/")
    @Extension(value = "json")
    @FileName(value = "FirstFileGenerated")
    private String jsonWriter(){
        return getJsonDummy();
    }


    private static String getJsonDummy() {
        return "{ " +
                "\"Task\" : \"Annotation\"" +
                "\"Team\" : \"Quartz\"" +
                "\"Level\" : \"Advanced\"" +
                " }";
    }

    @RunnableMethod
    @DefaultPath(value = "CHANGE ME /home/user/Desktop/SamplePath/")
    @Extension(value = "txt")
    @FileName(value = "SecondFileGenerated")
    private String nameDoesntMatterForThisTask(){
        return "This is a normal String written into a file Dummy";
    }


}
