package com.progressoft.interns.filecreator;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.List;

public abstract class FileCreator {

    // must be private
    private Path fileCreationImplementation(Path path, String fileName, String extension, Method method) {
        // Business of creating the file
        return null;
   }


   // must be public
    public List<Path> createFile(){
        // business of getting the annotations and using reflection API to invoke the methods of any class the inherits from this class
        return null;
    }




}
