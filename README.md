# Annotations Basic With Reflection API


## Goal

 - Deep understanding of Reflection API
 - Creating Annotations

## Objective

 Create the following Annotations
 - DefaultPath that contains a String value
 - Extension that contains a String value
 - FileName that contains a String value

 Run Main java application and check that the files are created successfully

### Steps to help with the implementation
1. DefaultPath - Extension - FileName should be declared on methods that contains RunnableMethod annotations
2. Modify the FileCreator createFile() method to check if any method contains RunnableMethod annotation
3. if the Method contains RunnableMethod and DefaultPath is not defined -> throw an exception
4. if the Method contains RunnableMethod and Extension is not defined -> throw an exception
5. if the Method contains RunnableMethod and FileName is not defined -> throw an exception
6. if all the annotations are present, call the fileCreationImplementation method and pass the correct values to the method
7. make sure that fileCreationImplementation is invoking the methods that contains RunnableMethods Annotations correctly
8. if one of the methods that has RunnableMethod annotation does not return a String -> throw an exception
9. After invoking the method and creating a file, return its Path
10. Save the Path to an ArrayList
11. if the returned paths are empty -> throw an exception
12. return the paths of all created files so the user can see the paths

### Summary:  
Modify the FileCreator Abstract Methods to read the annotations of its children classes  
and create the wanted files depending on the annotations created



# Optional
Make the DefaultPath value dynamic instead of being a fixed value  
Make the FileName value dynamic instead of being a fixed value


